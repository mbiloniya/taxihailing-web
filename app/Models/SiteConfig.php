<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteConfig extends Model
{
    public $timestamps = false;
	public static function boot()
	{
		parent::boot();
		static::updating(function ($model)
		{
			$model->updated_at = time();
		});
		
	}
	public function Rules($request)
	{

		$rules = [
			'header_script'	=>	'nullable',
			'footer_script'	=> 	'nullable'
		];
		return $rules;

	}
	public function getConfigs(){
    	$config = $this->all();

    	$result = array();
    	foreach ($config as $each) {
    		$result[$each->key] = $each;
    	}

    	return json_decode(json_encode($result));
    }
    public static function get($key)
    {	
           return SiteConfig::where('key',$key)->first();
    }
}
