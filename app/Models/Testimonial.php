<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model {
	protected $fillable = ['client_name', 'company_name', 'image_url', 'message', 'status', 'created_at', 'updated_at', 'deleted_at'];

	public $timestamps = false;

	public static function getTestimonialModel($limit, $offset, $search, $orderby,$order)
	{
		$orderby  = $orderby ? $orderby : 'updated_at';
		$order    = $order ? $order : 'desc';

		$q        = Testimonial::where('id', '!=', '');

		if($search && !empty($search)){
			$q->where(function($query) use ($search) {
				$query->where('client_name', 'LIKE', $search.'%');
			});
		}
		$response['total']	=	$q->count();
		$response['data']   =   $q->orderBy($orderby, $order)
		->offset($offset)
		->limit($limit)
		->get();

		$response   =   json_decode(json_encode($response));
		return $response;
	}

}

