<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Testimonial;
use Valid;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends Controller
{

    public function __construct()
    {
        $this->sortable_columns = [
            0 => 'client_name',
            1 => 'created_at',
        ];
    }
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
//

    if($request->ajax())
    {
        $limit          = $request->input('length');
        $start          = $request->input('start');
        $search         = $request['search']['value'];
        $orderby        = $request['order']['0']['column'];
        $order          = $orderby != "" ? $request['order']['0']['dir'] : "";
        $draw           = $request['draw'];

        $response       = Testimonial::getTestimonialModel($limit, $start, $search, $this->sortable_columns[$orderby], $order);



        if(!$response){
            $testimonials       = [];
            $totalUser     = [];
        }
        else{
            $testimonials = $response->data;
            $totalUser     = $response->total;
        }

        $testimonialData = array();
        $i = 1;
        foreach ($testimonials as $testimonial) {
            $u['id']            = $i+$start;
            $u['client_name']     = ucfirst($testimonial->client_name);
            $u['updated_at']    = date(env('TABLEDATE_FORMAT_PHP','M d, Y') ,strtotime($testimonial->updated_at));

            $test_status        = view('admin.testimonials.status_icon', [
                'id'     => $testimonial->id , 
                'status' => $testimonial->status
            ]);
            $u['status']        = $test_status->render();

            $actions            = view('admin.testimonials.actions', [
                'id'    => $testimonial->id
            ]);
            $u['actions']       = $actions->render(); 

            $testimonialData[] = $u;
            $i++;
            unset($u);
        }

        $return = [
            "draw"              =>  intval($draw),
            "recordsFiltered"   =>  intval( $totalUser),
            "recordsTotal"      =>  intval( $totalUser),
            "data"              =>  $testimonialData
        ];
        return $return;
    }



    $page_title = "Admin | Manage Testimonial";
    return view('admin.testimonials.index')
    ->with('page_title', $page_title);
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
    $page_title = "Admin | New Testimonials";
    return view('admin.testimonials.create')
    ->with('page_title', $page_title);
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request) {

    $input = $request->all();

    $status = 0;

    if ($request->has('status')) {
        $input['status'] = $request->status;
    }

    $validator =  Validator::make($input,[
        'client_name'       =>  getRule('client_name', true),
        'image'             =>  getRule('image', true),
        'message'           =>  getRule('message', true),

    ]);

    if($validator->fails()) {
        $errors = $validator->errors();

        $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
        return back()->withErrors($errors)->withInput();
    } else {

        //Upload image
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp . '-' . uniqid() . '-' . str_replace([' ', ':'], '-', $file->getClientOriginalName());

            Storage::putFileAs('public/testimonial_images', $file, $name);
            $path = Storage::url('testimonial_images/'.$name);

            $input['image_url'] = $path;

        }

        $testimonial = Testimonial::create($input);

        $request->session()->flash('alert-success', "Testimonial Created Successfully.  ");
        return redirect()->route('testimonials.index');
    }

}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
//
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id) {
    $id = decrypt($id);

    $response = Testimonial::where('id', $id)->first();;

    if (!$response) {
        \Request::session()->flash('alert-warning', $response->message);
        return redirect()->back();
    } else {
        $testimonial = $response;

        return view("admin.testimonials.edit")
        ->with('testimonial', $testimonial)
        ->with('page_title', 'Edit Testimonial');
    }
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id) {

    $id = decrypt($id);

    $input = $request->all();
    $validator =  Validator::make($input,[
        'client_name'       =>  getRule('client_name', true),
        'message'           =>  getRule('message', true),

    ]);

    if($validator->fails()) {
        $errors = $validator->errors();

        $request->session()->flash('alert-danger', 'Errors! Please correct the following errors and submit again.');
        return back()->withErrors($errors)->withInput();
    } else {
        // Update
        $testimonial = Testimonial::find($id);

        if ($request->has('client_name')) {
            $testimonial->client_name = $input['client_name'];

        }
        if ($request->has('company_name')) {
            $testimonial->company_name = $input['company_name'];
        }
        if ($request->has('message')) {
            $testimonial->message = $input['message'];
        }

        // Upload Imagess
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp . '-' . uniqid() . '-' . str_replace([' ', ':'], '-', $file->getClientOriginalName());

            Storage::putFileAs('public/testimonial_images', $file, $name);
            $path = Storage::url('testimonial_images/'.$name);

            if (!empty($testimonial->image_url)) {

                File::delete(public_path() . $testimonial->image_url);

            }

            $testimonial->image_url = $path;

        }

        if ($request->has('status')) {
            $testimonial->status = $input['status'];
        }

        $testimonial->save();
        $request->session()->flash('alert-success', 'Testimonial Updated Successfully.');
        return redirect()->route('testimonials.index');
    }
}

/**
* Update Testimonial type status - activate / deactivate.
*
* @param  int  $id
* @param  int  $status
* @return \Illuminate\Http\Response
*/
public function updateStatus($id, $status, Request $request) {
    $id = decrypt($id);
    try{
        $testimonial = Testimonial::find($id);
        $testimonial->status = $status;
        $testimonial->save();

        $activate_message = $status == 1 ? 'activated' : 'deactivated';

        $response = array(
            'status'    => 'success',
            'message'   => 'Comment successfully '.$activate_message,
        );

    }
    catch(ModelNotFoundException $e){
        $response = array(
            'status'    => 'failed',
            'message'   => 'Comment  not found.'
        );
    }

    return \Response::json($response);
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy(Request $request, $id)
{
    //
    $id = decrypt($id);
    try {
        $testimonial = Testimonial::findOrFail($id);
        if (!empty($testimonial->image_url)) {

            File::delete(public_path('images/testimonial/images') . "/" . basename($testimonial->image_url));
        }

        $testimonial->delete();
        $request->session()->flash('alert-success', 'Testimonial Deleted Successfully.');
        return response(['msg' => 'Content Deleted', 'status' => 'success']); 
    } catch (\Exception $e) {

        $request->session()->flash('alert-warning', 'Testimonial has not been removed because it uses the system.');
        return response(['msg' => 'Failed deleting the content', 'status' => 'failed']);
    }
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroyImage($id, Request $request) {

    try {
        $testimonial = Testimonial::find($id);
        if (!empty($testimonial->image_url)) {

            File::delete(public_path() . $testimonial->image_url);

            $testimonial->image_url = '';
            $testimonial->save();
        } else {
            throw new ModelNotFoundException();
        }

        $response = array(
            'status' => 'success',
            'message' => 'Image successfully deleted.',
        );

    } catch (ModelNotFoundException $e) {
        $response = array(
            'status' => 'failed',
            'message' => 'Image not found.',
        );
    }
    return $response;
}
}
