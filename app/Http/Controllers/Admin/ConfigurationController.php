<?php

namespace App\Http\Controllers\Admin;

use App\Models\SiteConfig;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SiteConfig $siteConfig)
    {
       $data = $siteConfig->getConfigs();
       $config['header_script']   = $data->header_script->value;
       $config['footer_script']   = $data->footer_script->value;
       $page_title = "Admin | Configuration";
       return view('admin.config.index')
       ->with('config', $config)
       ->with('page_title', $page_title);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,SiteConfig $siteConfig)
    {
        $input = $request->all();
        $rules= $siteConfig->Rules($request);
        $validator =  Validator::make($input,$rules);
        if ($validator->fails()) {
            $request->session()->flash('alert-danger', ('Error! Please fix following errors first.'));
            $errors = $validator->errors();
            return back()->withErrors($errors)->withInput();
        }
        foreach ($input as $key => $value) {
            if($key != "_token"){
                $config= $siteConfig::where('key', '=', $key)->first();
                $config->value=$value;
                $config->save();
            }
        }
         $request->session()->flash('alert-success', 'Setting updated successfully.');
        return redirect("admin/config"); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteConfig  $siteConfig
     * @return \Illuminate\Http\Response
     */
    public function show(SiteConfig $siteConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteConfig  $siteConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteConfig $siteConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteConfig  $siteConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteConfig $siteConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteConfig  $siteConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteConfig $siteConfig)
    {
        //
    }
}
