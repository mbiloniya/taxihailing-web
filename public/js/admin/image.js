/* multiple Image Upload */
var i =1;var j = 2;
$(document).on('change','#image',function(){

	var count = $('.file-preview-frame').length;

	if($(this).val()!=""){

		if(count < 5){

			var $this = $(this);

			if (typeof (FileReader) != "undefined") {
				var dvPreview = $(".file-preview .file-preview-thumbnails");
				var regex = /^([a-zA-Z0-9\s_\\.\- ():])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
				$($(this)[0].files).each(function () {
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {

						$clone = $this.clone(true);
						$clone.val('');
						$this.hide();

						$this.attr('id', 'uploadimage'+i);

						$clone.attr('data-rel', j);
						$clone.insertAfter($this); 
						var id = $this.data('rel');
						$(".file-preview").css("display","block");
						var reader = new FileReader();
						reader.onload = function (e) {
							var div_frame = document.createElement("div");
							div_frame.setAttribute("class",'file-preview-frame');
							var img = document.createElement("img");
							img.setAttribute('src', e.target.result);
							img.setAttribute('style', "height:160px;width:160px;");
							img.setAttribute("data-rel", id);
							img.setAttribute("class", "file-preview-image");

							var div_footer = document.createElement("div");
							div_footer.setAttribute('class', 'file-thumbnail-footer');
							div_footer.innerHTML = '<div class="file-actions"><div class=" file-footer-buttons"><button type="button" class="kv-file-remove btn btn-xs btn-default image_remove" title="Remove file"><i class="fa fa-trash-o text-danger" aria-hidden="true"></i></button></div><div class="clearfix"></div></div></div>';
							div_frame.append(img);
							div_frame.append(div_footer);

							dvPreview.append(div_frame);
						}
						reader.readAsDataURL(file[0]);
					} else {
						$this.val('');
						alert(file[0].name + " is not a valid image file.");

						return false;
					}
				});
			} else {
				alert("This browser does not support HTML5 FileReader.");
			}
			j++;i++;

		}else{

			alert("You can only upload a maximum of 5 files.");
			$(this).val('');
			return false;
		}
	}

});

$(document).on('click','.kv-file-remove.btn.btn-xs.btn-default.image_remove',function(){

	if(confirm("Are you sure you want to remove this image?")){            
		var img_rel = $(this).closest('.file-preview-frame').find('img').data('rel');

		var div = $(this).closest(".file-preview-frame");
		div.remove();
		$('input[data-rel="' + img_rel + '"]').remove();
		val = $('.file-preview .file-preview-frame');

		if(val.length == 0){
			$(".file-preview").css("display","none");
		}
	}


});

// Single image upload
$(document).on('change','#banner_image',function(){

	if($(this).val()!=""){

		var $this = $(this);


		if (typeof (FileReader) != "undefined") {
			var dvPreview = $(".file-preview_banner .file-preview-thumbnails");
			dvPreview.html('');
			var regex = /^([a-zA-Z0-9\s_\\.\- ():])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
			$($(this)[0].files).each(function () {
				var file = $(this);
				if (regex.test(file[0].name.toLowerCase())) {
					var id = $this.data('rel');
					var reader = new FileReader();
					reader.onload = function (e) {
						var div_frame = document.createElement("div");
						div_frame.setAttribute("class",'file-preview-frame');
						var img = document.createElement("img");
						img.setAttribute('src', e.target.result);
						img.setAttribute('style', "height:160px;width:160px;");
						img.setAttribute("data-rel", id);
						img.setAttribute("class", "file-preview-image");

						var div_footer = document.createElement("div");
						div_footer.setAttribute('class', 'file-thumbnail-footer');
						div_footer.innerHTML = '<div class="file-actions"> <div class="file-footer-buttons"><button type="button" class="kv-file-remove btn btn-xs btn-default banner_image_remove" title="Remove file"><i class="fa fa-trash-o text-danger" aria-hidden="true"></i></button></div><div class="clearfix"></div></div></div>';
						div_frame.append(img);
						div_frame.append(div_footer);

						dvPreview.append(div_frame);
					}
					reader.readAsDataURL(file[0]);
				} else {
					$this.val('');
					alert(file[0].name + " is not a valid image file.");

					return false;
				}
			});
		} else {
			alert("This browser does not support HTML5 FileReader.");
		}
		j++;i++;

	}

});


$(document).on('click','.banner_image_remove',function(){

	if(confirm("Are you sure you want to remove this image?")){

		var div = $(this).closest(".file-preview-frame");
		div.remove();
		$('#banner_image').val('');

	}  

});

