<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key',100);
            $table->text('value')->nullable();
            $table->string('created_at', 40);
            $table->string('updated_at', 40);
        });
        DB::table('site_configs')->insert(
            array(
                'key'               => 'header_script',
                'value'             => '',
                'created_at'        => time(),
                'updated_at'        => time()
            )
        );
        DB::table('site_configs')->insert(
            array(
                'key'               => 'footer_script',
                'value'             => '',
                'created_at'        => time(),
                'updated_at'        => time()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_configs');
    }
}
