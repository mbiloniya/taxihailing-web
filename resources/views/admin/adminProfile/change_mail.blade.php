
          <form method="post" action="{{ url('admin/update_email') }}" class="" id="emailchange">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class=" control-label required">New Email</label>
              
                <input id="email" type="email"  name="email" class="form-control" value="{{ Auth::user()->email }}"  placeholder="Email" maxlength="100" required>
          </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
              <label for="Password" class=" control-label required">Password
              </label>
                <input id="password" type="password"  name="password" class="form-control" value=""  placeholder="Password" maxlength="100" required>
          </div>
          <div class="modal-footer">
              <i class="fa fa-spin fa-refresh" id="loading" style="display:none;"></i>	
              <button type="button" id="submit" class="btn btn-default">Change</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              
            </div>
        </form>