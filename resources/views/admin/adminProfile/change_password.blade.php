<form method="post" action="{{ url('admin/change_password') }}" id="passwordChange">
    {{ csrf_field() }}

    <div class="form-group">
      <label for="current_password" class=" control-label required">Current Password</label>

        <input id="current_password" type="password"  name="current_password" class="form-control" placeholder="Current Password" required>
  </div>

   <div class="form-group">
      <label for="Password" class=" control-label required">New Password 
      <span class="glyphicon glyphicon-info-sign" title="The password must be at least 8 characters with 1 letter and 1 number."></span>
      </label>
      

        <input id="password" type="password"  name="password" class="form-control" placeholder="New Password" required>
  </div>

    <div class="form-group">
      <label for="password_confirmation" class="control-label required">Confirm Password</label>
      
      <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
      
  </div>
  <div class="modal-footer">         

      <button type="button" id="pwdsubmit" class="btn btn-primary">Save</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      
    </div>
  </form>