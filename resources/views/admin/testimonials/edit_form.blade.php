@include('admin.partials.errors')
<!-- form start -->
<form role="form" method="post" class="adsform" action="{{ route('testimonials.edit', encrypt($testimonial->id)) }}" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="box-body">

        <div class="form-group{{ $errors->has('client_name') ? ' has-error' : '' }}">
            <label for="name" class=" control-label required">Client Name </label>

            <input type="text" name="client_name" class="form-control" id="name" value="{{ $testimonial->client_name }}" maxlength="255" required>
            @if ($errors->has('client_name'))
            @foreach ($errors->get('client_name') as $msg)
            <span class="help-block">
                <strong>{{ $msg }}</strong>
            </span>
            @endforeach
            @endif

        </div>

        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
            <label for="company" class=" control-label">Company Name </label>

            <input type="text" name="company_name" class="form-control" id="company" value="{{ $testimonial->company_name }}" maxlength="255">
            @if ($errors->has('company_name'))
            @foreach ($errors->get('company_name') as $msg)
            <span class="help-block">
                <strong>{{ $msg }}</strong>
            </span>
            @endforeach
            @endif

        </div>

        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
            <label for="message" class=" control-label required">Message </label>

            <textarea class="textarea" id="page_description" name="message" style="width: 100%; min-height: 250px; font-size: 14px; border: 1px solid #dddddd; padding: 10px;" required>{{ $testimonial->message}}</textarea>

            @if ($errors->has('message'))
            @foreach ($errors->get('message') as $msg)
            <span class="help-block">
                <strong>{{ $msg }}</strong>
            </span>
            @endforeach
            @endif

        </div>
        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class=" control-label required">Image </label>
            <input type="file" name="image" class="form-control image_input" id="banner_image">

            <!-- <p class=" help-block ">Maximum file size 5MB.</p> -->
            @if ($errors->has('image'))
            <span class="help-block ">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
            @endif
        </div>

        <div class="form-group">
            <div class="file-input">
                <div class="file-preview_banner">

                    <div class="file-preview-thumbnails">
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        @if($testimonial->image_url)
        <div class="form-group">
            <div class="file-input">
                <div class="file-preview_bannerold" id="{{ $testimonial->id }}">

                    <div class="file-preview-thumbnails">
                        <div class="file-preview-frame">
                            <img src="{{ asset($testimonial->image_url) }}" class="file-preview-image" alt="Image Thumbnail" style="height:160px;width:160px;">
                            <div class="file-thumbnail-footer">

                                <div class="file-actions">
                                    <div class="file-footer-buttons">

                                        <button type="button" class="btn btn-xs btn-default" title="Remove file" onclick="deletepic('{{ $testimonial->id }}')"><i class="fa fa-trash-o text-danger" aria-hidden="true"></i></button>

                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        @endif
<!--         <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
         <label for="rating" class=" control-label required">Rating </label>
         <select id="rating" class="form-control" name="rating" required="required">
            <option value="1">
                1 &#9733;
            </option>
            <option value="2">
                2 &#9733;&nbsp;&#9733;
            </option>
            <option value="3">
                3 &#9733;&nbsp;&#9733;&nbsp;&#9733;
            </option>
            <option value="4">
                4 &#9733;&nbsp;&#9733;&nbsp;&#9733;&nbsp;&#9733;
            </option>
            <option value="5">
                5 &#9733;&nbsp;&#9733;&nbsp;&#9733;&nbsp;&#9733;&nbsp;&#9733;
            </option>
        </select>
        @if ($errors->has('rating'))
        @foreach ($errors->get('rating') as $msg)
        <span class="help-block">
          <strong>{{ $msg }}</strong>
      </span>
      @endforeach
      @endif
  </div> -->

  <div class="form-group">
    <label for="status" class="control-label required">Status</label>
    <select name="status" class="form-control">
        <option value="0" {{ old('status', $testimonial->status ) == 0 ? 'selected' : '' }} >Inactive</option>
        <option value="1" {{ old('status', $testimonial->status ) == 1 ? 'selected' : '' }}>Active</option>
    </select>
</div>

<!-- /.box-body -->
<div class="box-footer">
    <a href="{{ route('testimonials.index') }}" class="btn btn-default">Cancel</a>
    <button type="submit" class="btn btn-info pull-right">Save</button>
</div>

<!-- /.box-body -->

</div>
<!-- / form -->
</form>
