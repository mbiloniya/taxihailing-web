@extends('admin.layouts.layout')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Testimonials
		</h1>
		<ol class="breadcrumb">
			<li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active"><i class="fa fa-tags"></i> Testimonials</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Info boxes -->
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!-- <h3 class="box-title">Hover Data Table</h3> -->
						<a class="btn btn-social btn-primary" href="{!! url('admin/testimonials/create'); !!}">
							<i class="fa fa-plus"></i> Add Testimonial  
						</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						@include('admin.partials.errors')

						<table id="testTable" class="table table-bordered table-hover datatable">

							<thead>
								<tr>
									<th>Customer Name</th>
									<th>Updated At</th>
									<th class="action">Status</th>
									<th class="action">Action</th>
								</tr>
							</thead>

							<tfoot>
								<tr>
									<th>Customer Name</th>
									<th>Updated At</th>
									<th>Status</th> 
									<th>Action</th>
								</tr>
							</tfoot>
						</table>

					</div>

					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			
		</div>

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')
<script type="text/javascript">

	$(document).ready(function() {
		$('#testTable').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"order": [[ 1, "desc" ]],
			"ajax":{
				url :"{{ url('admin/testimonials') }}",

				error: function(error){  
					//console.log(error);
					alert('Something went wrong');
				}
			},
			"aoColumns": [
			{ mData: 'client_name' },
			{ mData: 'updated_at' },
			{ mData: 'status' },
			{ mData: 'actions' },
			],
			"aoColumnDefs": [
			{ "bSortable": false, "aTargets": ['action'] }
			]
		});
	});


	/*activate / deactivate members*/
	$(document).on("click", ".updateStatus", function(e) {
		var $this_ID = '#'+ $(this).attr('id');
		var $this = $(document).find($this_ID);
		$this.hide();
		$(this).parent().find('div.spinner').show();
		var id = $this.data('member-id');
		var status = $this.data('status');

		$.ajax({
			type: "GET",
			url: "{{ url('admin/testimonials/update_status') }}/"+id+"/"+status,
			data: {},
			dataType: "json",
			success: function( msg ) {
				$this.parent().find('div.spinner').hide();
				$this.show(); 

				if(msg.status == 'success'){
					$this.toggleClass('fa-thumbs-up fa-thumbs-down');

					if(status == '1'){
						$this.data('status', 0);

						$this
						.attr('data-original-title', 'Click to Deactivate')
						.tooltip('fixTitle')
						.tooltip('show').tooltip('hide');
					}else{

						$this.data('status', 1);
						$this.attr('data-original-title', 'Click to Activate')
						.tooltip('fixTitle')
						.tooltip('show').tooltip('hide');
					}
				}
			}
		});
	});

</script>
@endsection