@extends('admin.layouts.layout')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Testimonials
            <small>Edit Testimonial</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{!! url('admin/testimonials') !!}"><i class="fa fa-bank"></i> Testimonial</a></li>
            <li class="active"> Edit Page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-xs-12 col-md-8">
                    <div class="box">
                        <div class="box-header">
                        <!-- <h3 class="box-title">Hover Data Table</h3> -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                          @include('admin.testimonials.edit_form')
                        </div>

                     </div>
                    <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')

<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

</script>
<script type="text/javascript">
    var baseUrl = "{{url('admin/testimonials')}}/";
</script>

<script src="{{ asset('js/admin/image.js') }}"></script>

<script>

/*delete banner images - ajax*/
function deletepic(id) {

    if (confirm("Are you sure you want to delete this image?")) {
        $.ajax({
            type: "GET",
            url: "{{ url('admin/testimonials') }}/" + id + "/delete_image",
            data: {},
            dataType: "json",
            success: function (msg) {

                if (msg.status == 'success') {
                    $("#" + id).remove();
                    $("#banner_image").prop('required',true);
                } else {
                    alert(msg);
                }
            }
        });
    }
}

</script>
@endsection