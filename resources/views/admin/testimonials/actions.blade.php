<a href="{{ url('admin/testimonials/'.encrypt($id).'/edit') }}" class="btn btn-xs btn-success pencil_anchor" id='{{ encrypt($id) }}' title="Edit">
<i class="fa fa-pencil"></i>
</a>            

<a href="javascript:void(0)" id='{{"testimonials/destroy_".encrypt($id) }}' id='{{encrypt($id) }}' class='btn btn-xs btn-danger del' data-msg="Are you sure you want to delete this?" title="Delete"><i class="fa fa-trash"></i></a>