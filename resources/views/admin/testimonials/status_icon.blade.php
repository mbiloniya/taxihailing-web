<div class="box without-border spinner">
  <div class="overlay">
    <i class="fa fa-refresh fa-spin"></i>
  </div>
</div>

@if($status == '1')                        
    <i class="fa fa-2x fa-thumbs-up updateStatus" data-toggle="tooltip" title="Click to Deactivate" data-status="0" data-member-id="{{ encrypt($id) }}" id="status_{{ str_random(10) }}"></i>
@else
    <i class="fa fa-2x fa-thumbs-down updateStatus" data-toggle="tooltip" title="Click to Activate" data-status="1" data-member-id="{{ encrypt($id) }}" id="status_{{ str_random(10) }}"></i>
@endif