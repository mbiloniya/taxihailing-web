@extends('admin.layouts.layout')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Configuration
			<small>Website Setting</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{!! url('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Configuration</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Info boxes -->
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Update website settings</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						@include('admin.partials.errors')
						<form role="form" method="post" action="{{ route('config.store') }}">
							{{ csrf_field() }}
							<div class="form-group{{ $errors->has('header_script') ? ' has-error' : '' }}">
								<label for="site_description">Header Site Script</label>

								<textarea class="form-control" rows="3" id="header_script" name="header_script" placeholder="Header Site Script">{{$config['header_script']}}</textarea>

								@if ($errors->has('header_script'))
								<span class="help-block">
									<strong>{{ $errors->first('header_script') }}</strong>
								</span>
								@endif
							</div>


							<div class="form-group{{ $errors->has('footer_script') ? ' has-error' : '' }}">
								<label for="site_description">Footer Site Script</label>

								<textarea class="form-control" rows="3" id="footer_script" name="footer_script" placeholder="Footer Site Script">{{$config['footer_script']}}</textarea>

								@if ($errors->has('footer_script'))
								<span class="help-block">
									<strong>{{ $errors->first('footer_script') }}</strong>
								</span>
								@endif
							</div>
							<button type="submit" class="btn btn-info pull-right">Update</button>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>

		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection